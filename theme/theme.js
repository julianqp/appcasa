import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#515151',
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      light: red[500],
      main: red[500],
      dark: red[500]
    },
    warning: {
      main: '#ff9800',
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
