import React from "react";
import PropTypes from "prop-types";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Link from "next/link";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import GroupIcon from "@material-ui/icons/Group";
import SportsSoccerIcon from "@material-ui/icons/SportsSoccer";
import EmojiEventsIcon from "@material-ui/icons/EmojiEvents";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  texto: {
    margin: 0,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const MenuLateral = ({ open }) => {
  const classes = useStyles();
  const theme = useTheme();

  const opciones = [
    {
      icon: <GroupIcon />,
      text: "Usuario",
      url: "usuario",
    },
    {
      icon: <EmojiEventsIcon />,
      text: "Ingresos",
      url: "ingresos",
    },
    {
      icon: <SportsSoccerIcon />,
      text: "Gestiones",
      url: "gestiones",
    },
  ];

  return (
    <div>
      <List>
        {opciones.map((item) => {
          let texto = null;
          if (open) {
            texto = (
              <ListItemText className={classes.texto} primary={item.text} />
            );
          }
          return (
            <Link key={item.text} href={item.url}>
              <ListItem button>
                <ListItemIcon>{item.icon}</ListItemIcon>
                {texto}
              </ListItem>
            </Link>
          );
        })}
      </List>
    </div>
  );
};

MenuLateral.propTypes = {
  open: PropTypes.bool.isRequired,
};

export default MenuLateral;
