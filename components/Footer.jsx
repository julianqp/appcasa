import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import theme from "../theme/theme";
import Grid from "@material-ui/core/Grid";
import Copyright from "@material-ui/icons/Copyright";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    height: "100%",
    display: "flex",
    textAlign: "center",
    alignItems: "center",
  },
  paper: { width: "50%" },
  texto: { color: "white", margin: "5px 0" },
  container: {},
  menu: {},
  menuItem: {},
}));

const Footer = () => {
  const classes = useStyles(theme);

  return (
    <div className={classes.root}>
      <div className={classes.paper}>
        <Typography
          className={classes.texto}
          variant="caption"
          display="block"
          gutterBottom
        >
          &copy; Copuright 2020
        </Typography>
      </div>
      <div className={classes.paper}>
        <Typography
          className={`${classes.texto}`}
          variant="caption"
          display="block"
          gutterBottom
        >
          Incidencias
        </Typography>
        <Typography
          className={`${classes.texto}`}
          variant="caption"
          display="block"
          gutterBottom
        >
          Mejoras
        </Typography>
      </div>
    </div>
  );
};

export default Footer;
