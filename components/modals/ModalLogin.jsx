import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Boton from '../inputs/Boton';
import Input from '../inputs/Input'

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const Modal = ({ title, open, setOpen }) => {
  
   const [username, setUsername] = useState('');
  const [pass, setPass] = useState('');

  const usernameField = {
    error: false, label: 'Usuario', helperText: '', placeholder: 'Nombre de usuario', value: username, setValue: setUsername
  }

  const passField = {
    error: false, type: 'password', label: 'Contraseña', helperText: '', placeholder: 'Contraseña', value: pass, setValue: setPass
  }

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title" onClose={handleClose}>{title}</DialogTitle>
        <DialogContent>
          <>
            <Input items={usernameField} />
            <Input items={passField} />
        </>
        </DialogContent>
        <DialogActions>
          <>
            <Boton texto={'Candelar'} color={'secondary'} />
            <Boton texto={'Entrar'} color={'primary'} />
          </>
        </DialogActions>
      </Dialog>
    </div>
  );
}

Modal.propTypes = {
    title: PropTypes.string,
    open: PropTypes.bool.isRequired,
    setOpen: PropTypes.func.isRequired,
}

Modal.defaultProps  = {
    title: 'Modal',
}

export default Modal;