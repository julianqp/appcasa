import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { green, red } from "@material-ui/core/colors";
import Switch from "@material-ui/core/Switch";

const GreenSwitch = withStyles({
  switchBase: {
    color: red[300],
    "&$checked": {
      color: green[500],
    },
    "&$checked + $track": {
      backgroundColor: green[500],
    },
  },
  checked: {},
  track: {},
})(Switch);

const Switches = ({ name, estado, handleChange }) => {
  return (
    <div>
      <GreenSwitch
        checked={estado}
        onChange={handleChange}
        name={name}
        inputProps={{ "aria-label": "primary checkbox" }}
      />
    </div>
  );
};

export default Switches;
