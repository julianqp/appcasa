import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Seleccionable from "./inputs/Seleccionable";
import Boton from "./inputs/Boton";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    marginLeft: "20px",
    marginBottom: "20px",
    marginRight: "20px",
    marginTop: "20px",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  opciones: {
    display: "flex",
    maxWidth: "100%",
  },
  centrado: {
    display: "flex",
    justifyContent: "center",
  },
});

const Main = () => {
  const classes = useStyles();
  const [year, setYear] = useState("");
  const [compe, setCompe] = useState("");
  const [team, setTeam] = useState("");
  const [loaded, setLoaded] = useState(false);
  const years = ["2018", "2019", "2020"];
  const compes = ["La liga", "Premier", "Serie A"];
  const teams = ["Barcelona", "Madrid", "Roma"];

  const arr = [1, 2, 3, 4, 11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34];

  const load = () => setLoaded(!loaded);

  return (
    <div>
      <Card className={classes.root}>
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Word of the Day
          </Typography>
          <Grid className={`${classes.opciones}`} container spacing={3}>
            <Seleccionable
              options={years}
              value={year}
              changeValue={setYear}
              label={"Año"}
            />
            <Seleccionable
              disabled={!year}
              options={compes}
              value={compe}
              changeValue={setCompe}
              label={"Competición"}
            />
            <Seleccionable
              disabled={!compe}
              options={teams}
              value={team}
              changeValue={setTeam}
              label={"Equipo"}
            />
          </Grid>
        </CardContent>
        <CardActions>
          <Boton onClick={load} texto="Buscar" />
        </CardActions>
      </Card>
      {loaded ? (
        <Card className={classes.root}>
          <CardContent>
            <Grid className={`${classes.centrado}`} container spacing={3}>
              <Partidos color={"inserted"} key={1} />
              <Partidos key={2} />
              <Partidos color={"inserted"} key={3} />
              <Partidos key={4} />
            </Grid>
          </CardContent>
        </Card>
      ) : null}
    </div>
  );
};

export default Main;

const useStyless = makeStyles((theme) => ({
  root: {
    minWidth: 200,
    maxWidth: 250,
    margin: "10px",
    padding: "5px 0",
    backgroundColor: theme.primary,
  },
  title: {
    fontSize: 12,
    textAlign: "center",
  },
  pos: {
    marginBottom: 12,
  },
  inserted: {
    backgroundColor: "#f0f4c3",
  },
  insert: {
    backgroundColor: "#b3e5fc",
  },
}));

const Partidos = ({ color = "insert" }) => {
  const classes = useStyless();

  return (
    <Card className={`${classes.root} ${classes[color]}`}>
      <Typography className={classes.title}>Real Madrid</Typography>
      <Typography className={classes.title} color="textSecondary">
        VS
      </Typography>
      <Typography className={classes.title}>Real Madrid</Typography>
    </Card>
  );
};
