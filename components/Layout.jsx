import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Link from "next/link";
import Appbar from "../components/Appbar";
import MenuLateral from "../components/MenuLateral";
import Footer from "../components/Footer";

const Layout = ({ children }) => {
  const [open, setOpen] = React.useState(false);
  return (
    <div className="container item">
      <div className="header item">
        <Appbar open={open} setOpen={setOpen} />
      </div>
      <div className="menu item">
        <MenuLateral open={open} />
      </div>
      {children}
      <div className="footer item">
        <Footer />
      </div>
    </div>
  );
};

export default Layout;
