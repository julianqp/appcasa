import React from "react";
import PropTypes from "prop-types";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import { red, green, grey, yellow, blue } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: red,
  },
});

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const Boton = ({ texto, color, onClick }) => {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <Button onClick={onClick} variant="outlined" color={color}>
          {texto}
        </Button>
      </div>
    </ThemeProvider>
  );
};

Boton.prototype = {
  texto: PropTypes.string.isRequired,
  color: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Boton.defaultProps = {
  color: "default",
};

export default Boton;
