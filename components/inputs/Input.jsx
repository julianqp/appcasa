import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 200,
    },
  },
}));

const Input = ({ items }) => {
  const {
    error,
    label,
    helperText,
    type,
    placeholder,
    value,
    setValue,
  } = items;
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <TextField
          type={type}
          onChange={(e) => setValue(e.target.value)}
          error={error}
          id="standard-error-helper-text"
          label={label}
          placeholder={placeholder}
          value={value}
          helperText={helperText}
        />
      </div>
    </form>
  );
};

Input.prototype = {
  items: PropTypes.shape({
    type: PropTypes.string,
    error: PropTypes.bool,
    label: PropTypes.string.isRequired,
    helperText: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string.isRequired,
      PropTypes.number.isRequired,
    ]),
    setValue: PropTypes.func.isRequired,
  }),
};

Input.defaultProps = {
  type: "text",
  error: false,
  placeholder: "",
  helperText: "",
};
export default Input;
