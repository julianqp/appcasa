import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Seleccionable = ({
  disabled,
  helpText,
  label,
  value,
  changeValue,
  options,
}) => {
  const classes = useStyles();
  return (
    <div>
      <FormControl className={classes.formControl} disabled={disabled}>
        <InputLabel>{label}</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          value={value}
          onChange={(e) => changeValue(e.target.value)}
        >
          {options.map((item) => (
            <MenuItem key={item} value={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>{helpText}</FormHelperText>
      </FormControl>
    </div>
  );
};

Seleccionable.propTypes = {
  disabled: PropTypes.bool,
  helpText: PropTypes.string,
  label: PropTypes.string.isRequired,
  options: PropTypes.array,
  value: PropTypes.string.isRequired || PropTypes.number.isRequired,
  changeValue: PropTypes.func.isRequired,
};

Seleccionable.defaultProps = {
  options: [],
  disabled: false,
  helpText: "",
};

export default Seleccionable;
