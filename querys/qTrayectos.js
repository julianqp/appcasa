import gql from "graphql-tag";

export const GET_GASTOS = gql`
  {
    getGastos {
      _id
      fecha
      concepto
      tipo
      cantidad
      pagado
    }
  }
`;

export const GET_USUARIO = gql`
  query getUsuario($email: String) {
    getUsuario(email: $email) {
      _id
      nombre
      apellidos
      usuario
      email
      casas {
        actual
        old
      }
      viajes {
        actual
        old
      }
      otros {
        actual
        old
      }
      dateCreate
      notificaciones {
        addGastos
        liquidacion
        informe
      }
    }
  }
`;

export const SET_GASTO = gql`
  mutation createGasto(
    $concepto: String
    $tipo: Tipo
    $cantidad: Float
    $pagado: String
  ) {
    createGasto(
      concepto: $concepto
      tipo: $tipo
      cantidad: $cantidad
      pagado: $pagado
    ) {
      _id
      fecha
      concepto
      tipo
      cantidad
      pagado
    }
  }
`;

export const SET_NOTIFICACION = gql`
  mutation changeNotificaciones($clave: String, $valor: Boolean) {
    changeNotificaciones(clave: $clave, valor: $valor) {
      _id
    }
  }
`;
