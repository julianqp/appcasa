import React from "react";
import Layout from "../components/Layout";
import Main from "../components/Main";

const Partidos = () => {
  return (
    <Layout>
      <div className="main item">
        <Main />
      </div>
    </Layout>
  );
};

export default Partidos;
