import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Seleccionable from "../components/inputs/Seleccionable";
import Boton from "../components/inputs/Boton";
import Layout from "../components/Layout";
import Grid from "@material-ui/core/Grid";
import json from "../api/top_players.json";
import TablaIngresos from "../components/Tablas/TablaIngresos";
import Input from "../components/inputs/Input";
import { SET_GASTO, GET_GASTOS } from "../querys/qTrayectos";
import { useMutation, useQuery } from "@apollo/react-hooks";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    marginLeft: "20px",
    marginBottom: "20px",
    marginRight: "20px",
    marginTop: "20px",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  opciones: {
    display: "flex",
    maxWidth: "100%",
  },
  centrado: {
    display: "flex",
    justifyContent: "center",
  },
});

const getToken = () => null;

const Ingresos = () => {
  const classes = useStyles();
  const [concepto, setConcepto] = useState("");
  const [tipo, setTipo] = useState("");
  const [cantidad, setCantindad] = useState("");
  const [pagado, setPagado] = useState("");
  const [data, setData] = useState([]);
  const [setGasto, datos] = useMutation(SET_GASTO);
  const { loading, error, refetch } = useQuery(GET_GASTOS, {
    onCompleted: ({ getGastos }) => {
      setData(getGastos);
    },
  });

  const insertar = async (e) => {
    e.preventDefault();
    const res = await setGasto({
      variables: { concepto, tipo, cantidad: parseFloat(cantidad), pagado },
    });
    if (res.data) {
      setCantindad("");
      setConcepto("");
      setTipo("");
      setPagado("");
      refetch({
        onCompleted: ({ getGastos }) => {
          setData(getGastos);
        },
      });
    }
  };

  const borrar = async () => {
    setCantindad("");
    setConcepto("");
    setTipo("");
    setPagado("");
  };

  const itemsConcepto = {
    value: concepto,
    label: "Concepto",
    type: "text",
    setValue: setConcepto,
  };
  const itemsTipo = {
    value: tipo,
    label: "Tipo",
    type: "text",
    setValue: setTipo,
  };
  const itemsCantidad = {
    value: cantidad,
    label: "Cantidad",
    type: "number",
    setValue: setCantindad,
  };
  const itemsPagado = {
    value: pagado,
    label: "Pagado por",
    type: "text",
    setValue: setPagado,
  };
  const tipos = ["compartido", "singular"];

  return (
    <Layout>
      <div className="main item">
        <div>
          <Card className={classes.root}>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Introduzca los datos de una nuevo ingreso
              </Typography>
              <Grid className={`${classes.opciones}`} container spacing={3}>
                <Input items={itemsConcepto} />
                <Seleccionable
                  options={tipos}
                  value={tipo}
                  changeValue={setTipo}
                  label={"Tipo"}
                />
                <Input items={itemsCantidad} />
                <Input items={itemsPagado} />
              </Grid>
            </CardContent>
            <CardActions>
              <Boton onClick={(e) => insertar(e)} texto="Insertar" />
              <Boton onClick={() => borrar()} texto="Borrar" />
            </CardActions>
          </Card>

          <Card className={classes.root}>
            <CardContent>
              <TablaIngresos datos={data ? data : []} />
            </CardContent>
          </Card>
        </div>
      </div>
    </Layout>
  );
};

export default Ingresos;

const useStyless = makeStyles((theme) => ({
  root: {
    minWidth: 200,
    maxWidth: 250,
    margin: "10px",
    padding: "5px 0",
    backgroundColor: theme.primary,
  },
  title: {
    fontSize: 12,
    textAlign: "center",
  },
  pos: {
    marginBottom: 12,
  },
  inserted: {
    backgroundColor: "#f0f4c3",
  },
  insert: {
    backgroundColor: "#b3e5fc",
  },
}));

const Partidos = ({ color = "insert" }) => {
  const classes = useStyless();
  return (
    <Card className={`${classes.root} ${classes[color]}`}>
      <Typography className={classes.title}>Real Madrid</Typography>
      <Typography className={classes.title} color="textSecondary">
        VS
      </Typography>
      <Typography className={classes.title}>Real Madrid</Typography>
    </Card>
  );
};

const Jugador = ({ color = "insert", name = "Arnulfo" }) => {
  const classes = useStyless();

  return (
    <Card className={`${classes.root} ${classes[color]}`}>
      <Typography className={classes.title}>{name}</Typography>
    </Card>
  );
};
