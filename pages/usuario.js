import React, { useState } from "react";
import { getCookies, setCookies, removeCookies } from "cookies-next";
import { makeStyles } from "@material-ui/core/styles";
import { useMutation, useQuery } from "@apollo/react-hooks";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Seleccionable from "../components/inputs/Seleccionable";
import Boton from "../components/inputs/Boton";
import Layout from "../components/Layout";
import Grid from "@material-ui/core/Grid";
import json from "../api/top_players.json";
import { GET_USUARIO, SET_NOTIFICACION } from "../querys/qTrayectos";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Home from "@material-ui/icons/Home";
import CardTravel from "@material-ui/icons/CardTravel";
import Store from "@material-ui/icons/Store";
import Receipt from "@material-ui/icons/Receipt";
import Switch from "../components/Switch";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    marginLeft: "20px",
    marginBottom: "20px",
    marginRight: "20px",
    marginTop: "20px",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {},
  pos: {
    marginBottom: 12,
  },
  opciones: {
    display: "flex",
    maxWidth: "100%",
  },
  centrado: {
    display: "flex",
    justifyContent: "center",
  },
  linea: {
    width: "100%",
    display: "flex",
  },
  titulo: {
    width: "20%",
    fontSize: 14,
    fontWeight: "bold",
  },
  contenido: {
    width: "80%",
  },
  parrafo: {
    margin: "auto 0",
    width: "90%",
  },
});

const Usuario = ({ user }) => {
  const classes = useStyles();
  const [perfil, setPerfil] = useState(null);
  const [loaded, setLoaded] = useState(false);
  const [setNotificacion, notificacion] = useMutation(SET_NOTIFICACION);
  const [state, setState] = React.useState({
    addGastos: false,
    liquidacion: false,
    informe: false,
  });
  const { loading, error, refetch } = useQuery(GET_USUARIO, {
    variables: { email: "julian.querol.p@gmail.com" },
    onCompleted: ({ getUsuario }) => {
      setPerfil({ usuario: { ...getUsuario } });

      setState({
        addGastos: getUsuario.notificaciones.addGastos,
        liquidacion: getUsuario.notificaciones.liquidacion,
        informe: getUsuario.notificaciones.informe,
      });
    },
  });

  const handleChange = async (event) => {
    event.preventDefault();
    const { name, checked } = event.target;
    const res = await setNotificacion({
      variables: { clave: name, valor: checked },
    });
    console.log(name, checked);
    if (res.data.changeNotificaciones._id) {
      setState({ ...state, [name]: checked });
    }
  };

  const load = () => setLoaded(!loaded);

  let contenido = <div>No data</div>;

  if (error) {
    contenido = <div>Error</div>;
  }
  if (loading) {
    contenido = <div>Cargando</div>;
  }
  if (perfil) {
    const { usuario } = perfil;
    contenido = (
      <div className="main item">
        <div>
          <Card className={classes.root}>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Datos de usuario
              </Typography>
              <div className={classes.linea}>
                <Typography gutterBottom className={`${classes.titulo}`}>
                  Nombre:
                </Typography>
                <Typography gutterBottom className={`${classes.contenido}`}>
                  {usuario.nombre}
                </Typography>
              </div>
              <div className={classes.linea}>
                <Typography gutterBottom className={`${classes.titulo}`}>
                  Apellidos:
                </Typography>
                <Typography gutterBottom className={`${classes.contenido}`}>
                  {usuario.apellidos}
                </Typography>
              </div>
              <div className={classes.linea}>
                <Typography gutterBottom className={`${classes.titulo}`}>
                  Email:
                </Typography>
                <Typography gutterBottom className={`${classes.contenido}`}>
                  {usuario.email}
                </Typography>
              </div>
              <div className={classes.linea}>
                <Typography gutterBottom className={`${classes.titulo}`}>
                  Usuario:
                </Typography>
                <Typography gutterBottom className={`${classes.contenido}`}>
                  {usuario.usuario}
                </Typography>
              </div>
              <div className={classes.linea}>
                <Typography gutterBottom className={`${classes.titulo}`}>
                  Fecha:
                </Typography>
                <Typography gutterBottom className={`${classes.contenido}`}>
                  {usuario.dateCreate}
                </Typography>
              </div>
            </CardContent>
            <CardActions>
              <Boton texto="Cambiar contraseña" />
            </CardActions>
          </Card>
          <Card className={classes.root}>
            <CardContent>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Notidicaciones
              </Typography>
              <div className={classes.linea}>
                <p className={classes.parrafo}>Gasto a nombre añadido</p>
                <Switch
                  name="addGastos"
                  estado={state.addGastos}
                  handleChange={handleChange}
                />
              </div>
              <div className={classes.linea}>
                <p className={classes.parrafo}>Liquidación de deuda</p>
                <Switch
                  name="liquidacion"
                  estado={state.liquidacion}
                  handleChange={handleChange}
                />
              </div>
              <div className={classes.linea}>
                <p className={classes.parrafo}>Informe mensual</p>
                <Switch
                  name="informe"
                  estado={state.informe}
                  handleChange={handleChange}
                />
              </div>
            </CardContent>
          </Card>
          <Card className={classes.root}>
            <CardContent>
              <Listado
                casas={usuario.casas}
                viajes={usuario.viajes}
                otros={usuario.otros}
              />
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }

  return <Layout>{contenido}</Layout>;
};

Usuario.getInitialProps = async (ctx) => {
  const nombre = getCookies(ctx, "user");

  return { user: nombre };
};

export default Usuario;

const useStylesList = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const Listado = ({ casas, viajes, otros }) => {
  const classes = useStylesList();

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Agrupaciones
        </ListSubheader>
      }
      className={classes.root}
    >
      <Desplegable tipo="Casas" datos={casas} />
      <Desplegable tipo="Viajes" datos={viajes} />
      <Desplegable tipo="Otros" datos={otros} />
    </List>
  );
};

const Desplegable = ({ tipo, datos }) => {
  const classes = useStylesList();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  let icon;

  switch (tipo) {
    case "Casas":
      icon = <Home />;
      break;
    case "Viajes":
      icon = <CardTravel />;
      break;
    case "Otros":
      icon = <Store />;
      break;
    default:
      icon = <Receipt />;
      break;
  }

  if (datos && (datos.actual || (datos.old && datos.old.lenght > 0))) {
    return (
      <>
        <ListItem button onClick={handleClick}>
          <ListItemIcon>{icon}</ListItemIcon>
          <ListItemText primary={tipo} />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {datos.actual ? (
              <ItemList
                icon={icon}
                text={datos.actual}
                clases={classes.nested}
              />
            ) : null}

            {datos.old ? (
              <>
                {datos.old.map((elem) => (
                  <ItemList icon={icon} text={elem} clases={classes.nested} />
                ))}
              </>
            ) : null}
          </List>
        </Collapse>
      </>
    );
  }

  return <ItemList icon={icon} text={tipo} />;
};

const ItemList = ({ icon, text, clases = "" }) => (
  <ListItem key={text} className={clases}>
    <ListItemIcon>{icon}</ListItemIcon>
    <ListItemText primary={text} />
  </ListItem>
);
